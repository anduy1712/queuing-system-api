import ENV from '@config/environtment';
import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

export const errorHandlingMiddleware = (err: any, req: Request, res: Response, next: NextFunction) => {
  if (!err.statusCode) err.statusCode = StatusCodes.INTERNAL_SERVER_ERROR;

  const responseError = {
    statusCode: err.statusCode,
    message: err.message || StatusCodes[err.statusCode],
    stack: err.stack
  };

  console.log('ENV.BUILD_MODE', ENV.BUILD_MODE);
  if (ENV.BUILD_MODE !== 'dev') delete responseError.stack;

  res.status(responseError.statusCode).json(responseError);
};
