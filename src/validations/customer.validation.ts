import Joi from 'joi';
import { StatusCodes } from 'http-status-codes';
import { NextFunction, Request, Response } from 'express';

const customerSchema = Joi.object({
  name: Joi.string().min(3).max(30),
  phone: Joi.string().min(3).max(30)
  //   queueId: ObjectId,
  //   ticketId: ObjectId,
  //   createdAt: Date,
  //   status: Joi.string().min(3).max(30)
});

const createNew = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const value = await customerSchema.validateAsync(req.body, { abortEarly: false });
    next();
  } catch (error: FixMeLater) {
    res.status(StatusCodes.BAD_REQUEST).json({
      errors: new Error(error || {}).message
    });
  }
};

export const customerValidation = {
  createNew
};
