import { CONNECT_DB, GET_DB } from '@config/mongodb';
import express, { NextFunction, Request, Response } from 'express';
import exitHook from 'async-exit-hook';
import apiV1 from './routes/v1';
import { errorHandlingMiddleware } from './middlewares/errorHandling.middleware';

const startServer = () => {
  const app = express();
  const HOST = 'localhost';
  const PORT = 3000;

  // Enable request body
  app.use(express.json());

  // API
  app.use('/v1', apiV1);

  app.listen(PORT, HOST, () => {
    console.log('hello 2');
  });

  // Catch Error
  app.use(errorHandlingMiddleware);

  exitHook(() => {
    process.exit(0);
  });
};

CONNECT_DB()
  .then(() => {
    console.log('connect to database successful');
  })
  .then(() => {
    startServer();
  })
  .catch((error) => {
    console.log('error', error);
    process.exit(0);
  });
