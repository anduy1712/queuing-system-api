import ApiError from '@utils/apiError';
import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

const createNew = async (req: Request, res: Response, next: NextFunction) => {
  try {
    throw new ApiError(StatusCodes.OK, 'userController: created customer successful');
  } catch (error: FixMeLater) {
    next(error);
  }
};

export const userController = {
  createNew
};
