import express from 'express';
import { StatusCodes } from 'http-status-codes';
import { userController } from '~/controllers/user.controller';
import { customerValidation } from '~/validations/customer.validation';

const customerRoute = express.Router();

customerRoute
  .route('/')
  .get((req, res) => {
    res.status(StatusCodes.CREATED).json({ message: 'customerRoute created' });
  })
  .post(customerValidation.createNew, userController.createNew);

export default customerRoute;
