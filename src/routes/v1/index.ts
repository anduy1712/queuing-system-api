import express from 'express';
import { StatusCodes } from 'http-status-codes';
import customerRoute from '~/routes/v1/customer.route';

const apiV1 = express.Router();

apiV1.get('/status', (req, res) => {
  res.status(StatusCodes.OK).json({ message: 'success' });
});

// API Customer
apiV1.use('/customer', customerRoute);

export default apiV1;
