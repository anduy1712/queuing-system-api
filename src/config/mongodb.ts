import { Db, MongoClient, ServerApiVersion } from 'mongodb';
import ENV from './environtment';

let queueingDatabase: Db | null = null;

const client = new MongoClient(ENV.URI, {
  serverApi: { version: ServerApiVersion.v1, strict: true, deprecationErrors: true }
});

export const CONNECT_DB = async () => {
  await client.connect();

  queueingDatabase = client.db(ENV.DATABASE_NAME);
};

export const GET_DB = () => {
  if (!queueingDatabase) throw Error('Must connect to database first');

  return queueingDatabase;
};
