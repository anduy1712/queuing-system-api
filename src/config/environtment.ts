import 'dotenv/config';

const ENV = {
  HOST: process.env.HOST || 'localhost',
  PORT: process.env.PORT || 8017,
  DATABASE_NAME: process.env.DATABASE_NAME || '',
  URI: process.env.URI || '',
  BUILD_MODE: process.env.BUILD_MODE
};

export default ENV;
